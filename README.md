![](src/resources/static/images/logo-messenger.png)

# Messenger Application

This project is just to experiment and pratice sockets implementing a Client and Chat Server.

# Features
 * A Chat Server ✔︎
 * Chat Clients ✔︎
 * Login Interface to access the chat server ✘
 * Chat Client can create rooms and invite clients ✘
 
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

You'll need :
* 15 min of your precious time.
* Some good motivation.
* Java
* Docker

### Prerequisites

Everything has been wrapped inside a Docker container so, you'll just need Docker.

You can build a tagged docker image using the command line like this:
``` sh
$ dockerfile:build
```

### Installing

Upcoming ...
<!---
A step by step series of examples that tell you have to get a development env running

Say what the step will be:

``` sh
./mvnw install
```
Excecute the application:
``` sh
./mvn spring-boot:run
```
Producte the jar file:
``` sh
./mvn clean package && java -jar ./target/restful_webservice-1.0.0-SNAPSHOT.jar
```
--->
<!---
And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo
--->
## Running the tests

...Up coming soon with JUnit
<!---
Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```
-->
### And coding style tests
...Upcoming soon with Checkstyle
<!---
Explain what these tests test and why

```
Give an example
```
--->
## Deployment
...Upcoming Soon
<!---
Add additional notes about how to deploy this on a live system
--->
## Built With

* [Docker](https://www.docker.com/) - Docker

* [Java Sockets](https://docs.oracle.com/javase/tutorial/networking/sockets/index.html)

## Contributing
...Upcoming Soon
<!---
Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.
--->
## Versioning
...Upcoming Soon
<!---
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 
--->
## Authors

* **Tanawa Tsamo Marius** - *Messenger Application* - [Codecritics](https://gitlab.com/codecritics/)
<!---
See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.
--->
## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Packt Tutorials
* Some Books
* Motivation after day work...