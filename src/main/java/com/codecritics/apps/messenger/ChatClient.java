package com.codecritics.apps.messenger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Vector;

public class ChatClient extends JFrame implements Runnable {

    Socket socket;

    JTextArea jTextArea;
    JButton sendButton, logoutButton;
    JTextField jTextField;

    Thread thread;


    DataInputStream din;
    DataOutputStream dout;

    String LoginName;

    public static void main(String[] args) throws IOException {
        ChatClient client = new ChatClient("Codecritics_1");
    }

    public ChatClient(String login) throws IOException {
        super(login);
        LoginName = login;

        jTextArea = new JTextArea(18, 50);
        jTextField = new JTextField(50);

        sendButton = new JButton("Send");
        logoutButton = new JButton("Logout");

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dout.writeUTF(LoginName + " " + " DATA " + jTextField.getText());
                    jTextField.setText("");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dout.writeUTF(LoginName + " " + " LOGOUT ");
                    System.exit(1);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        socket = new Socket("localhost", 5217);

        din = new DataInputStream(socket.getInputStream());
        dout = new DataOutputStream(socket.getOutputStream());

        dout.writeUTF(LoginName);
        dout.writeUTF(LoginName + " " + "LOGIN");

        thread = new Thread(this);
        thread.start();
        setup();
    }

    private void setup() {
        setSize(600, 400);

        JPanel jPanel = new JPanel();

        jPanel.add(new JScrollPane(jTextArea));
        jPanel.add(jTextField);
        jPanel.add(sendButton);
        jPanel.add(logoutButton);
        add(jPanel);

        setVisible(true);
    }

    @Override
    public void run() {
        while (true) {
            try {
                jTextArea.append("\n" + din.readUTF());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
